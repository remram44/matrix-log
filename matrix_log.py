import aiohttp
import asyncio
from datetime import datetime
import getpass
import json
import sys
from urllib.parse import quote


HOMESERVER = ''
USER = ''
TOKEN = ''


http_session = None


term_lock = asyncio.Lock()


def ainput(prompt):
    return asyncio.get_event_loop().run_in_executor(
        None,
        input, prompt,
    )


def agetpass(prompt='Password: '):
    return asyncio.get_event_loop().run_in_executor(
        None,
        getpass.getpass, prompt,
    )


async def json_req(method, path, content=None, params=None):
    if method not in ('GET', 'POST', 'PUT'):
        raise ValueError("Invalid method %r" % method)
    headers = {}
    if content is not None:
        headers['Content-Type'] = 'application/json'
        content = json.dumps(content)
    request = getattr(http_session, method.lower())(
        HOMESERVER + '/_matrix/client/r0' + path,
        data=content,
        headers=headers,
        params=params,
    )
    async with request as response:
        return await response.json()


def format_time(event):
    ts = event['origin_server_ts'] / 1000
    dt = datetime.fromtimestamp(ts)
    return dt.strftime('%Y-%m-%d %H:%M:%S')


async def login():
    async with term_lock:
        print("matrix_log is not configured!")
        username = await ainput("User name: ")
        password = await agetpass()
        content = dict(type='m.login.password', device_id='matrix-log',
                       user=username, password=password)
        resp = await json_req('POST', '/login', content)
        print("Please set the USER variable to {!r}".format(resp['user_id']))
        print("Please set the TOKEN variable to {!r}"
              .format(resp['access_token']))


async def main():
    # Do initial sync
    sync_filter = '{"room": {"timeline": {"limit": 20}}}'
    response = await json_req('GET', '/sync',
                              params={'user_id': USER, 'access_token': TOKEN,
                                      'filter': sync_filter})
    rooms = {}
    for room_id, sync_room in response['rooms']['join'].items():
        room = rooms[room_id] = {'name': None}
        room['prev_batch'] = sync_room['timeline']['prev_batch']
        room['events'] = sync_room['timeline']['events']
        for event in sync_room['state']['events']:
            if 'type' not in event:
                continue
            if event['type'] == 'm.room.name':
                room['name'] = event['content'].get('name')

    # Select a room
    async with term_lock:
        if not rooms:
            print("No rooms.")
            return

        print("Rooms:")
        selection = []
        for i, (room_id, room) in enumerate(rooms.items()):
            print("  {: 3d}) {} ({})".format(i, room['name'], room_id))
            selection.append(room_id)
        while True:
            sel = await ainput(
                "Which room? (0-{}) ".format(len(selection) - 1)
            )
            try:
                sel = int(sel)
            except ValueError:
                pass
            else:
                if 0 <= sel < len(selection):
                    room_id = selection[sel]
                    room = rooms[room_id]
                    break

        print("Selected: {}".format(room['name']))

    # Process events into a list of messages
    messages = []

    async def add_event(event):
        if event['type'] == 'm.room.message':
            # This is a message, add to the list
            messages.insert(0, event)
        elif event['type'] == 'm.room.member':
            if (event['content'].get('membership') == 'join' and
                    event['sender'] == USER):
                # This is the point at which we joined the room. Do we want to
                # go further?
                async with term_lock:
                    r = ''
                    while r not in ('y', 'n'):
                        r = input("You joined this room {}. Keep going? "
                                  "(y/n) ".format(format_time(event)))
                        r = r.lower()
                    if r == 'n':
                        return False
        elif event['type'] == 'm.room.create':
            # Room creation, first event in the room
            return False
        return True

    async with term_lock:
        print("Getting events from room {}...".format(room_id))

    stop = False
    for event in reversed(room['events']):
        if not await add_event(event):
            stop = True
            break

    prev = room['prev_batch']

    while not stop and len(messages) < 100:
        resp = await json_req('GET',
                              '/rooms/{}/messages'.format(quote(room_id)),
                              params={'user_id': USER, 'access_token': TOKEN,
                                      'roomId': room_id, 'from': prev,
                                      'dir': 'b', 'limit': 100})
        if not resp['chunk']:
            break
        for event in resp['chunk']:
            if not await add_event(event):
                stop = True
                break
        prev = resp['end']

    async with term_lock:
        print("Messages from room {}".format(room['name']))
        for event in messages:
            if not event['content']:
                content = '<redacted>'
            else:
                content = event['content'].get('body', '')
            print("{} {} {}".format(
                format_time(event),
                event.get('sender'),
                content,
            ))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    http_session = aiohttp.ClientSession()
    try:
        if not HOMESERVER:
            print("matrix_log is not configured!")
            print("Please set the HOMESERVER variable, "
                  "for example 'https://matrix.org")
            sys.exit(2)
        elif not (USER and TOKEN):
            loop.run_until_complete(loop.create_task(login()))
            sys.exit(2)
        else:
            loop.run_until_complete(loop.create_task(main()))
    finally:
        loop.run_until_complete(loop.create_task(
            http_session.__aexit__(None, None, None)
        ))
        loop.stop()
